import React from "react";
// used for making the prop types of this component

// reactstrap components
import { Container, Nav, NavItem, NavLink } from "reactstrap";

class Footer extends React.Component {
  render() {
    return (
      <footer className="footer">
        <Container fluid>
          <Nav>
            <NavItem>
              <NavLink target="_blank" href="https://gitlab.com/omendra98">
                Gitlab
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                target="_blank"
                href="https://docs.google.com/document/d/1Nk9fMJXncgpnoaFaOlhH7jproMLEsubx3IJHoa9IOzs/edit?usp=sharing"
              >
                My Resume
              </NavLink>
            </NavItem>
          </Nav>
          <div className="copyright">
            © {new Date().getFullYear()}
            Vizitors
          </div>
        </Container>
      </footer>
    );
  }
}

export default Footer;
