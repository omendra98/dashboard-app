import React from "react";

import DataTable, { createTheme } from "react-data-table-component";

createTheme("solarized", {
  text: {
    primary: "#c0c1c2",
    secondary: "#fff",
  },
  background: {
    default: "#212529",
  },
  context: {
    background: "#cb4b16",
    text: "#FFFFFF",
  },
  divider: {
    default: "#32325d",
  },
  action: {
    button: "rgba(0,0,0,.54)",
    hover: "rgba(0,0,0,.08)",
    disabled: "rgba(0,0,0,.12)",
  },
});

const data = [
  {
    id: 1,
    title: "Dakota Rice",
    country: "Niger",
    city: "Oud-Turnhout",
    salary: "$36,738",
  },
  {
    id: 2,
    title: "Minerva Hooper",
    country: "Curaçao",
    city: "Sinaai-Waas",
    salary: "$23,789",
  },
  {
    id: 3,
    title: "Sage Rodriguez",
    country: "Niger",
    city: "Oud-Turnhout",
    salary: "$26,738",
  },
  {
    id: 4,
    title: "Philip Chaney",
    country: "Niger",
    city: "Oud-Turnhout",
    salary: "$46,738",
  },
  {
    id: 5,
    title: "Doris Greene",
    country: "Niger",
    city: "Oud-Turnhout",
    salary: "$25,671",
  },
  {
    id: 6,
    title: "Mason Porter",
    country: "Niger",
    city: "Oud-Turnhout",
    salary: "$46,718",
  },
  {
    id: 7,
    title: "Jon Porter",
    country: "Niger",
    city: "Oud-Turnhout",
    salary: "$12,789",
  },
  {
    id: 8,
    title: "Omi",
    country: "India",
    city: "Delhi",
    salary: "$20,738",
  },
  {
    id: 9,
    title: "Mani Karan",
    country: "India",
    city: "Oud-Turnhout",
    salary: "$66,738",
  },
  {
    id: 10,
    title: "Abhishek",
    country: "New york",
    city: "Oud-Turnhout",
    salary: "$36,738",
  },
];
const columns = [
  {
    name: "Name",
    selector: "title",
    sortable: true,
  },
  {
    name: "Country",
    selector: "country",
    right: true,
  },
  {
    name: "City",
    selector: "city",
    right: true,
  },
  {
    name: "Salary",
    selector: "salary",
    sortable: true,
    right: true,
  },
];

class Data extends React.Component {
  render() {
    return (
      <DataTable
        title="Employee Data"
        columns={columns}
        data={data}
        pagination="true"
        paginationPerPage="5"
        responsive="true"
        theme="solarized"
      />
    );
  }
}

export default Data;
