# React Dashboard

This project uses the following technologies:

- [React]
- [Express]
- [MongoDB]
- [Redux]


## Configuration

Make sure to add your own `MONGOURI` from your [mLab](http://mlab.com) database in `config/keys.js`.

```javascript
module.exports = {
  mongoURI: "YOUR_MONGO_URI_HERE",
  secretOrKey: "secret"
};
```

## Quick Start
```javascript
// Install dependencies for server
npm install 

//Install dependencies for client
npm run client-install

//Run server
npm run server

//Run client(Make sure you are in client directory)
npm run client

// Run client & server with concurrently
npm run dev

// Server runs on http://localhost:5000 and client on http://localhost:3000
```

**App is currently deployed here:-** (https://fierce-sierra-62178.herokuapp.com/)